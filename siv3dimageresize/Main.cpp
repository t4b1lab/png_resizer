﻿# include <opencv2/opencv.hpp>
# include <Siv3D.hpp> // OpenSiv3D v0.6.5

void Main()
{
	Window::Resize(400, 300);
	// 背景の色を設定 | Set background color
	Scene::SetBackground(ColorF{ 0.7, 0.8, 1.0 });

	// 通常のフォントを作成 | Create a new font
	const Font font40{ 40 };
	const Font font20{ 20 };

	// 絵文字用フォントを作成 | Create a new emoji font
	const Font emojiFont{ 60, Typeface::ColorEmoji };

	// `font` が絵文字用フォントも使えるようにする | Set emojiFont as a fallback
	font40.addFallback(emojiFont);

	Texture nun{ U"resources/nun.png" };



	const std::vector<Vec2> resize_properties = {
		Vec2(1280, 720),
		Vec2(1920,1080),
		Vec2(2540,1440),
		Vec2(3840,2160)
	};

	Vec2 current_resize_property = resize_properties[0];
	while (System::Update())
	{
		nun.draw(0, 0, ColorF(0.8, 0.7));
		font40(U"PNG resizer").draw(0, 0);
		String t = U"resize to : {}x{}"_fmt(current_resize_property.x, current_resize_property.y);
		font20(U"D&D png here").draw(200, 120);
		font20(t).draw(10, 250);
		std::vector<bool> resize_buttons = {
			SimpleGUI::Button(U"1280x720", Vec2{ 10, 60 }, 120),
			SimpleGUI::Button(U"1920x1080", Vec2{ 10, 110 }, 120),
			SimpleGUI::Button(U"2560x1440", Vec2{ 10, 160 }, 120),
			SimpleGUI::Button(U"3840x2160", Vec2{ 10, 210 }, 120),
		};


		for (int i = 0; i < static_cast<int>(resize_buttons.size()); i++)
		{
			if (resize_buttons[i])
			{
				current_resize_property = resize_properties[i];
			}
		}
		if (DragDrop::HasNewFilePaths())
		{
			const auto dropped_file = DragDrop::GetDroppedFilePaths();
			for (const auto& drop : dropped_file)
			{
				auto path = drop.path;
				Image image(path);
				auto scaled = image.scaled(current_resize_property.x, current_resize_property.y);
				auto path2 = FileSystem::ParentPath(path) + U"/" + FileSystem::BaseName(path) + U"_scaled_{}_{}"_fmt(current_resize_property.x, current_resize_property.y) + U".png";
				scaled.savePNG(path2);
			}
		}
	}
}

